import axios from 'axios'

const instance = axios.create({
  baseURL: '/api',
});

export default {
  createNew(name, email, summary, score, text) {
    return instance
      .post('/score', null, { params: {name, email, summary, score, text}})
      .then(response => response.status)
      .catch(err => console.warn(err));
  },

  getAll() {
    return instance.get('/score')
  }
}
