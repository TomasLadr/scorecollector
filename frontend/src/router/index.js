import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'App',
    component: function () {
		  return import('../App.vue')
		}
  }
]

const router = new VueRouter({
	mode: 'history',
  routes
})

export default router
