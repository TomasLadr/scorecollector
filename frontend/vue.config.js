module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
	devServer: {
	 proxy: {
	   '/api': {
	     target: 'http://localhost:8090' // this configuration needs to correspond to the Spring Boot backends' application.properties server.port
	   }
	 }
	}
}