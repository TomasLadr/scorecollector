### Score Collector

This is an application used for collection of user reviews of undefined service.  
The application is written in Spring Boot and VueJS.  
A live deployment is available on Heroku: https://score-collector.herokuapp.com/ 

#### Modules
This application contains two modules:
* backend: contains Spring Boot application
* frontend: contains VueJS application

#### Prerequisites
Ubuntu

```
sudo apt update
sudo apt install node
npm install -g @vue/cli
```

#### How to run it
In root directory type:
```
mvn clean install
java -Dserver.port=$PORT -jar backend/target/*.jar
```

\
\
\
author: Tomáš Ládr