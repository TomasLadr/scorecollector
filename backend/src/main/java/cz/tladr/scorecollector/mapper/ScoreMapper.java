package cz.tladr.scorecollector.mapper;

import cz.tladr.scorecollector.domain.Score;
import cz.tladr.scorecollector.dto.ScoreDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * <p>
 *     Class used for mapping between {@link Score} and {@link ScoreDto}
 * </p>
 *
 * @author tladr
 */
@Mapper(componentModel = "spring")
public interface ScoreMapper {

    ScoreMapper INSTANCE = Mappers.getMapper(ScoreMapper.class);

    /**
     * <p>
     *     Maps {@link Score} to {@link ScoreDto}
     * </p>
     *
     * @param score to be mapped
     * @return {@link ScoreDto} corresponding to passed Score
     */
    @Mapping(source = "author.name", target = "name")
    @Mapping(source = "author.email", target = "email")
    ScoreDto scoreToScoreDto(Score score);

    /**
     * <p>
     *     Maps {@link ScoreDto} to {@link Score}
     * </p>
     *
     * @param scoreDto to be mapped
     * @return {@link Score} corresponding to passed ScoreDto
     */
    @Mapping(source = "name", target = "author.name")
    @Mapping(source = "email", target = "author.email")
    Score scoreDtoToScore(ScoreDto scoreDto);
}
