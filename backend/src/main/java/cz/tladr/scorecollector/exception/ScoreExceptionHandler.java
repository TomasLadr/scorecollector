package cz.tladr.scorecollector.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ValidationException;

/**
 * <p>
 *     Class used for handling of exceptions.
 *     Contains own implementation of {@link ValidationException}.
 *     Handling of other exceptions is done by its parent class.
 * </p>
 *
 * @author tladr
 */
@ControllerAdvice
public class ScoreExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * <p>
     *     Handles {@link ValidationException}
     *     Generates response with code 400 and custom message.
     * </p>
     *
     * @param ex exception
     * @param request that caused the exception
     * @return response entity
     */
    @ExceptionHandler(value = ValidationException.class )
    public ResponseEntity<Object> handleValidationException(ValidationException ex, WebRequest request) {
        return handleExceptionInternal(ex, "Passed request is invalid!", new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
}
