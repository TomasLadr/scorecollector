package cz.tladr.scorecollector.dto;

import lombok.*;
import org.springframework.lang.Nullable;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * <p>
 *     DTO containing all info about a score
 * </p>
 *
 * @author tladr
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class ScoreDto {

    @NotNull
    @Min(value = 1)
    @Max(value = 5)
    private Integer score;

    @NotNull
    private String summary;

    @NotNull
    private String name;

    @NotNull
    @Email
    private String email;

    @Nullable
    private String text;
}
