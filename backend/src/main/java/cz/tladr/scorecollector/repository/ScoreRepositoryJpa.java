package cz.tladr.scorecollector.repository;

import cz.tladr.scorecollector.domain.Score;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *     Repository layer.
 * </p>
 *
 * @author tladr
 */
@Transactional
public interface ScoreRepositoryJpa extends JpaRepository<Score, Long> {

    /**
     * <p>
     *     Reads all scores from DB and orders them by ID
     *     Newest is the first
     * </p>
     *
     * @return List of all scores
     */
    @Transactional(readOnly = true)
    List<Score> findByOrderByCreatedDesc();
}
