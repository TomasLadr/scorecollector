package cz.tladr.scorecollector.service;

import cz.tladr.scorecollector.dto.ScoreDto;

import java.util.List;

/**
 * @author tladr
 */
public interface ScoreService {

    /**
     * <p>
     *     Reads all existing scores from DB
     * </p>
     *
     * @return List of existing scores
     */
    List<ScoreDto> findAll();

    /**
     * <p>
     *     Adds new score
     * </p>
     *
     * @param scoreDto to be added
     */
    void create(ScoreDto scoreDto);
}
