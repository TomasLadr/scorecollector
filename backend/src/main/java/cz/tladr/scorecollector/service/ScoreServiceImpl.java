package cz.tladr.scorecollector.service;

import cz.tladr.scorecollector.domain.Score;
import cz.tladr.scorecollector.dto.ScoreDto;
import cz.tladr.scorecollector.mapper.ScoreMapper;
import cz.tladr.scorecollector.repository.ScoreRepositoryJpa;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *     Service class for {@link cz.tladr.scorecollector.domain.Score operations}
 * </p>
 *
 * @author tladr
 */
@Slf4j
@Service
public class ScoreServiceImpl implements ScoreService {

    @Autowired
    private ScoreRepositoryJpa scoreRepositoryJpa;

    /**
     * <p>
     *     Reads all existing scores from DB
     * </p>
     *
     * @return List of existing scores
     */
    @Override
    public List<ScoreDto> findAll() {

        log.info("ScoreService: findAll() has been called");
        List<Score> s = scoreRepositoryJpa.findByOrderByCreatedDesc();
        for (Score sc:s
             ) {
            log.info(sc.toString());
        }
        List<ScoreDto> existingScores = scoreRepositoryJpa.findByOrderByCreatedDesc().stream().map(ScoreMapper.INSTANCE::scoreToScoreDto).collect(Collectors.toList());
        log.debug(String.format("Found %d scores found", existingScores.size()));
        return existingScores;
    }

    /**
     * <p>
     *     Adds new score
     * </p>
     *
     * @param scoreDto to be added
     */
    @Override
    public void create(ScoreDto scoreDto) {

        log.info("ScoreService: create() has been called");
        scoreRepositoryJpa.save(ScoreMapper.INSTANCE.scoreDtoToScore(scoreDto));
        log.debug(String.format("New score stored: %s", scoreDto.toString()));
    }
}
