package cz.tladr.scorecollector.controller;

import cz.tladr.scorecollector.dto.ScoreDto;
import cz.tladr.scorecollector.service.ScoreService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.ValidationException;
import java.util.List;

/**
 * <p>
 *     Controller layer of the application.
 * </p>
 *
 * @author tladr
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class ScoreController {

    @Autowired
    private ScoreService scoreService;

    /**
     * <p>
     *     Processes GET request.
     *     Returns ordered list of all existing scores.
     * </p>
     *
     * @return List of {@link ScoreDto}
     */
    @GetMapping("/score")
    public List<ScoreDto> getAllScore() {

        log.info("GET on /score");
        return scoreService.findAll();
    }

    /**
     * <p>
     *     Processes POST request.
     *     Validates the body sent.
     *     Than stores the score.
     * </p>
     *
     * @param newScoreDto bound DTO from the request
     */
    @PostMapping("/score")
    public void create(@Valid ScoreDto newScoreDto) {

        log.info("POST on /score: {}", newScoreDto.toString());
        scoreService.create(newScoreDto);
    }
}
