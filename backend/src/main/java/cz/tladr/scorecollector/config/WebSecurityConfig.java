package cz.tladr.scorecollector.config;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * <p>
 *     Class setting up security configuration.
 * </p>
 *
 * @author tladr
 */
@EnableWebSecurity
public class WebSecurityConfig  extends WebSecurityConfigurerAdapter {

    /**
     * <p>
     *     Sets up the the configuration.
     *     All request does not have to authorize.
     *     CSRF is disabled.
     * </p>
     *
     * @param http configuration object
     * @throws Exception when sth fails
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .authorizeRequests()
                .antMatchers("/**").permitAll()
                .and()
                .csrf().disable();
    }
}
