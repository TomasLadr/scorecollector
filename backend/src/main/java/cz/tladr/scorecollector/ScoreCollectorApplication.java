package cz.tladr.scorecollector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *     Main class.
 * </p>
 *
 * @author tladr
 */
@SpringBootApplication
public class ScoreCollectorApplication {

	public static void main(String[] args) {

		SpringApplication.run(ScoreCollectorApplication.class, args);
	}
}
