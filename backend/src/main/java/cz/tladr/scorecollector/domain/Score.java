package cz.tladr.scorecollector.domain;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.Calendar;

/**
 * <p>
 *     Entity mapping Score table.
 * </p>
 *
 * @author tladr
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "score")
public class Score extends AbstractEntity{

    public Score (Integer score, String summary, String text, Author author) {
        this.score = score;
        this.summary = summary;
        this.text = text;
        this.author = author;
    }

    @CreationTimestamp
    @EqualsAndHashCode.Exclude
    private LocalDateTime created;

    @NotNull
    private Integer score;

    @NotNull
    private String summary;

    private String text;

    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "author_id", nullable = false, updatable = false)
    private Author author;
}
