package cz.tladr.scorecollector.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

/**
 * <p>
 *     Entity mapping Author table.
 * </p>
 *
 * @author tladr
 */
@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@EqualsAndHashCode
@Entity
@Table(name= "author")
public class Author extends AbstractEntity {

    @NotNull
    private String name;

    @NotNull
    @Email
    private String email;
}
