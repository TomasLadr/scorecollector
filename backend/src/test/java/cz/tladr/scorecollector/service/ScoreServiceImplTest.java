package cz.tladr.scorecollector.service;

import cz.tladr.scorecollector.domain.Author;
import cz.tladr.scorecollector.domain.Score;
import cz.tladr.scorecollector.repository.ScoreRepositoryJpa;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;


/**
 * @author tladr
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ScoreServiceImplTest {

    private static final Score SCORE_1 = new Score(1, "summary1", "text1", new Author("name1", "email@test1.cz"));
    private static final Score SCORE_2 = new Score(2, "summary2", "text2", new Author("name2", "email@test2.cz"));

    @Autowired
    private ScoreServiceImpl service;

    @MockBean
    ScoreRepositoryJpa scoreRepositoryJpa;

    @Test
    public void getAllScoreEmptyTest() {

        given(scoreRepositoryJpa.findByOrderByCreatedDesc()).willReturn(Collections.emptyList());
        assertThat(service.findAll().isEmpty()).isTrue();
    }

    @Test
    public void getAllScoreMultipleScoresTest() {

        given(scoreRepositoryJpa.findByOrderByCreatedDesc()).willReturn(Arrays.asList(SCORE_1, SCORE_2));
        assertThat(scoreRepositoryJpa.findByOrderByCreatedDesc().size()).isEqualTo(2);
    }
}