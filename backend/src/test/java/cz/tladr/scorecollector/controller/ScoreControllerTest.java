package cz.tladr.scorecollector.controller;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.tladr.scorecollector.dto.ScoreDto;
import cz.tladr.scorecollector.service.ScoreService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author tladr
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(ScoreController.class)
class ScoreControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ScoreService service;

    @Test
    void getAllScoreTest() throws Exception {

        ScoreDto score1 = new ScoreDto(1, "summary1", "name1", "email@test1.cz", "text1");
        ScoreDto score2 = new ScoreDto(2, "summary2", "name2", "email@test2.cz", "text2");

        given(service.findAll()).willReturn(Arrays.asList(score1, score2));

        mvc.perform(get("/api/score"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$[0].score", is(score1.getScore())))
            .andExpect(jsonPath("$[1].summary", is(score2.getSummary())))
            .andExpect(jsonPath("$[0].name", is(score1.getName())))
            .andExpect(jsonPath("$[1].email", is(score2.getEmail())))
            .andExpect(jsonPath("$[0].text", is(score1.getText())));
    }

    @Test
    void createNewScoreValidTest() throws Exception {

        ScoreDto scoreDto = new ScoreDto(1, "summary1", "name1", "valid@email.cz", "text1");
        ArgumentCaptor<ScoreDto> captor = ArgumentCaptor.forClass(ScoreDto.class);

        int status = mvc.perform(post("/api/score?score=1&summary=summary1&name=name1&email=valid@email.cz&text=text1"))
                .andReturn().getResponse().getStatus();

        assertThat(200).isEqualTo(status);

        verify(service).create(captor.capture());
        assertThat(scoreDto).isEqualTo(captor.getValue());
    }
}