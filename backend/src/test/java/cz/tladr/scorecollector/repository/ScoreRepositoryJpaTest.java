package cz.tladr.scorecollector.repository;

import cz.tladr.scorecollector.domain.Author;
import cz.tladr.scorecollector.domain.Score;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author tladr
 */
@ExtendWith(SpringExtension.class)
@DataJpaTest
public class ScoreRepositoryJpaTest {

    @Autowired
    private ScoreRepositoryJpa scoreRepositoryJpa;

    private static final Score SCORE_1 = new Score(1, "summary1", "text1", new Author("name1", "email@test1.cz"));
    private static final Score SCORE_2 = new Score(2, "summary2", "text2", new Author("name2", "email@test2.cz"));

    @Test
    public void findByOrderByIdDescTest() {

        scoreRepositoryJpa.save(SCORE_1);
        scoreRepositoryJpa.save(SCORE_2);

        List<Score> orderedScore = scoreRepositoryJpa.findByOrderByCreatedDesc();

        // rows are ordered correctly
        assertThat(orderedScore.get(0).getId()).isEqualTo(2);
        assertThat(orderedScore.get(1).getId()).isEqualTo(1);

        assertThat(orderedScore.get(0)).isSameAs(SCORE_2);
        assertThat(orderedScore.get(1)).isSameAs(SCORE_1);
    }
}
